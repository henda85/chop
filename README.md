## Contexte du projet

J'ai conçu ce projet dans le cadre de mon stage,
j'ai utilisé next.js, tailwind CSS et firebase.

Pour voir le rendu :
git clone nomduprojet

npm i

voir le rendu sur localhost :

npm run dev

## Lien

[Lien du projet](https://chop-rho.vercel.app/)
