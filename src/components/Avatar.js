import React from "react";

function Avatar() {
  return (
    <div className=" ml-4 pt-4 mr-28 space-y-2 font-bold text-xl">
      <img
        loading="lazy"
        className={`h-36 w-36 rounded-full`}
        src="https://images.unsplash.com/photo-1624561172888-ac93c696e10c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8YXZhdGFyJTIwcHJvZmlsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=700&q=60"
        alt="profile pic"
      />
      <p> {"Nom de l'enseigne"}</p>
    </div>
  );
}

export default Avatar;
