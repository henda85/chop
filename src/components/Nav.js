import React from "react";
import { useState } from "react";
import Link from "next/link";
function Nav() {
  const [showLinks, setShowLinks] = useState(false);
  const handleShowLinks = () => {
    setShowLinks(!showLinks);
    console.log(showLinks);
  };

  return (
    <div>
      <nav className="bg-gray-100">
        <div className="max-w-6xl mx-auto ">
          <div className="flex justify-between ">
            <div className="flex space-x-4">
              {/**logo */}
              <div className="flex">
                <Link href="/">
                  <img
                    src="img/logo.png"
                    alt=""
                    className="h-7 ml-2 md:mt-4 md:mb-0 my-2"
                  />
                </Link>
                <p className="text-2xl pt-4">PRO</p>
              </div>
              {/*primery*/}
              <div className="hidden md:flex items-center space-x-1">
                <Link href="/">
                  <a className="py-5 px-3 text-gray-700 hover:text-gray-900 font-bold">
                    {"Accueil "}
                  </a>
                </Link>
                <Link href="/ajoutPage">
                  <a className="py-5 px-3 text-gray-700 hover:text-gray-900 font-bold">
                    {"Ajouter un article"}
                  </a>
                </Link>
              </div>
            </div>
            {/**secondary */}
            <div className="hidden md:flex items-center space-x-1">
              <Link href="/connexion">
                <a className="py-5 px-3 font-bold">Se connecter</a>
              </Link>
              <Link href="/inscription">
                <a
                  className="py-2 px-3 bg-pink-500 hover:bg-pink-600
                 rounded transation duration-300 font-bold"
                >
                  {"S'inscrire"}
                </a>
              </Link>
            </div>
            {/**nav button mobile */}
            <div className="md:hidden flex items-center">
              <button className="mobile-menu-button" onClick={handleShowLinks}>
                <svg
                  className="w-6 h-6"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                    clipRule="evenodd"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
          {/**nav mobile*/}

          <div className={`md:hidden ${showLinks ? "mobile-menue" : "hidden"}`}>
            <Link href="/ajoutPage">
              <a className="block py-2 px-4 text-sm hover:bg-gray-200 font-bold">
                {"Ajouter un article"}
              </a>
            </Link>
            <Link href="#">
              <a className="block py-2 px-4 text-sm hover:bg-gray-200 font-bold">
                {"S'abonner"}
              </a>
            </Link>
            <Link href="/connexion">
              <a className="block py-2 px-4 text-sm hover:bg-gray-200 font-bold">
                {"Se connecter"}
              </a>
            </Link>
            <Link href="/inscription">
              <a
                className="block py-2 text-sm font-bold
              
              px-4 hover:bg-yellow-300 text-yellow-900 hover:text-yellow-800
              rounded transation duration-300"
              >
                {"S'inscrire"}
              </a>
            </Link>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Nav;
