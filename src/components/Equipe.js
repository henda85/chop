import React from "react";

function Equipe() {
  return (
    <div className=" mt-12 grid grid-cols-1 md:grid-cols-3 gap-4">
      <div className="py-12 border-top border-t-2 border-solid border-gray-400 mx-4">
        <img
          src="https://thispersondoesnotexist.com/image"
          className="h-52 rounded-full mx-auto"
          alt="gérante"
        />
        <p className="text-xl text-center py-2"> Gérante: Marie</p>
        <p className="text-center">
          {
            "Lorem ipsum dolor sit amet consectetur,a ipisicing elit.Repellat sunt corrupti obcaecati dolorum laborum praesentium"
          }
        </p>
      </div>
      <div className="py-12 border-top border-t-2 border-solid border-gray-400 mx-4">
        <img
          src="https://thispersondoesnotexist.com/image"
          className="h-52 rounded-full mx-auto"
          alt="gérante"
        />
        <p className="text-xl text-center py-2"> Vendeuse: Marie</p>
        <p className="text-center">
          {
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat sunt corrupti obcaecati dolorum laborum praesentium"
          }
        </p>
      </div>
      <div className="py-12 border-top border-t-2 border-solid border-gray-400 mx-4">
        <img
          src="https://thispersondoesnotexist.com/image"
          className="h-52  mx-auto rounded-full"
          alt="vendeuse"
        />
        <p className="text-xl text-center py-2"> Vendeuse:</p>
        <p className="text-center">
          {
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat sunt corrupti obcaecati dolorum laborum praesentium"
          }
        </p>
      </div>
    </div>
  );
}

export default Equipe;
