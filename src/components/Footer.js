import Link from "next/link";
import React from "react";

function Footer() {
  return (
    <div className="">
      <div className="bg-gray-100 h-28">
        <div className="max-w-6xl mx-auto ">
          <div className="flex justify-start items-center pt-8">
            <Link href="/">
              <img src="img/logo.png" alt="" className="h-7 md:m-0 ml-2" />
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
