import React, { useState } from "react";
import Router from "next/router";
import { db } from "../utils/firebaseConfig";
import { collection, addDoc } from "firebase/firestore";

import router from "next/router";

function Ajout() {
  const [nom, setNom] = useState("");
  const [matiere, setMatiere] = useState("");
  const [prix, setPrix] = useState(0);
  const [photo, setPhoto] = useState("");
  const [discription, setDiscription] = useState("");

  let articlesCollectionref = collection(db, "articles");

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   const article = { nom, matiere, prix, photo };
  //   fetch("http://localhost:8100/articles", {
  //     method: "post",
  //     headers: { "Content-Type": "application/json" },
  //     body: JSON.stringify(article),
  //   }).then(() => {
  //     Router.push("/");
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();
    const article = { nom, matiere, prix, photo, discription };
    const createArticle = async () => {
      await addDoc(articlesCollectionref, article);
    };
    createArticle();
    router.push("/");
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-white">
      <div className="bg-white p-16 rounded shadow-2xl md:w-2/3 mt-6 mb-24">
        <h2 className="text-3xl font-bold mb-10 text-purple-800">
          Nouveau article
        </h2>
        <form onSubmit={handleSubmit} className="space-y-4">
          <div>
            <label
              className=" block mb-1 font-bold text-gray-500"
              htmlFor="nom"
            >
              Nom
            </label>
            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
              rounded outline-none focus:border-purple-500 "
              id="nom"
              value={nom}
              onChange={(e) => setNom(e.target.value)}
              required
            />
          </div>

          <div>
            <label
              className=" block mb-1 font-bold text-gray-500"
              htmlFor="matiere"
            >
              Matiere
            </label>
            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
              id="matiere"
              value={matiere}
              onChange={(e) => {
                setMatiere(e.target.value);
              }}
            />
          </div>
          <div>
            <label
              className=" block mb-1 font-bold text-gray-500"
              htmlFor="prix"
            >
              Prix
            </label>
            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
              id="prix"
              value={prix}
              onChange={(e) => {
                setPrix(e.target.value);
              }}
            />
          </div>
          <div>
            <label
              className=" block mb-1 font-bold text-gray-500"
              htmlFor="discription"
            >
              Description
            </label>
            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
              id="discription"
              value={discription}
              onChange={(e) => {
                setDiscription(e.target.value);
              }}
            />
          </div>
          <div>
            <label
              className=" block mb-1 font-bold text-gray-500"
              htmlFor="photo"
            >
              Photo
            </label>

            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
              id="photo"
              value={photo}
              onChange={(e) => {
                setPhoto(e.target.value);
              }}
            />
          </div>

          <button
            type="submit"
            className="text-white block bg-pink-500 rounded w-full hover:bg-pink-600 p-4 transition duration-300"
          >
            Terminer
          </button>
        </form>
      </div>
    </div>
  );
}

export default Ajout;
