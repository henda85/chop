import React from "react";
import Link from "next/link";
function Form() {
  return (
    <div className="min-h-screen flex items-center justify-center bg-white">
      <div className="bg-white p-16 rounded shadow-2xl md:w-2/3">
        <h2 className="text-3xl font-bold mb-10 text-purple-800">
          Créer votre compte
        </h2>
        <form className="space-y-4">
          <div>
            <label className=" block mb-1 font-bold text-gray-500">Nom</label>
            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
            />
          </div>
          <div>
            <label className=" block mb-1 font-bold text-gray-500">Email</label>
            <input
              type="email"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
            />
          </div>
          <div>
            <label className=" block mb-1 font-bold text-gray-500">
              Adresse
            </label>
            <input
              type="text"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
            />
          </div>
          <div>
            <label className=" block mb-1 font-bold text-gray-500">
              Mot de passe
            </label>
            <input
              type="password"
              className="w-full border-2 border-gray-400 p-3
               rounded outline-none focus:border-purple-500 "
            />
          </div>
          <div className="flex items-center">
            <input
              type="checkbox"
              id="accept"
              className="mr-2 text-sm text-gray-700"
            />
            <label htmlFor="accept">
              {"J'accepte les conditions d'utilisations"}
            </label>
          </div>
          <Link href="/">
            <button
              type="submit"
              className=" text-white block bg-pink-500 rounded w-full hover:bg-pink-600 p-4 transition duration-300   "
            >
              {"S'inscrire "}
            </button>
          </Link>
        </form>
      </div>
    </div>
  );
}

export default Form;
