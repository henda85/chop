import React from "react";
import {
  ClockIcon,
  LocationMarkerIcon,
  PhoneIcon,
} from "@heroicons/react/solid";

function Contact() {
  return (
    <div className="bg-white p-16 mt-6 space-y-4 rounded shadow-2xl ">
      <div className="flex space-x-2">
        <ClockIcon className="h-6" />
        <p> 10h-20h</p>
      </div>
      <div className="flex space-x-2">
        <LocationMarkerIcon className="h-6" />
        <p>103 Rue de la liberte Ville Cedex</p>
      </div>
      <div className="flex space-x-2">
        <PhoneIcon className="h-6" />
        <p>+33 6 66 66 66 66</p>
      </div>
    </div>
  );
}

export default Contact;
