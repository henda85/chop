import React from "react";
import Link from "next/link";
import { useParams } from "react-router-dom";

function DisplayArticle({ articles }) {
  return (
    <>
      <div className="min-h-screen  ">
        <div className=" grid  grid-cols-1 gap-4 md:grid-cols-4 px-12 ">
          {articles.map((item) => {
            return (
              <div key={item.id} className="">
                <div className="border-2 border-gray-300">
                  <img
                    src={item.photo}
                    alt=""
                    className=" h-52 w-52 rounded cursor-pointer transition duration-200 transform hover:scale-110 m-5"
                    objectFit="cover"
                  />
                  <div className="flex  flex-col space-x-2 space-y-2">
                    <div className=" ">
                      <p className="text-sm text-gray-900 font-bold mt-1 ml-2 capitalize">
                        {item.nom}
                      </p>
                      <p className=" ml-2 text-sm text-gray-900 font-bold mt-1 ">
                        prix {item.prix}€
                      </p>
                    </div>
                    <div className="">
                      <Link href={"/article/" + item.id}>
                        <button className=" mr-3 mb-5  rounded  py-1 px-2 bg-pink-500 hover:bg-pink-400 text-white">
                          Detail
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default DisplayArticle;
