import "tailwindcss/tailwind.css";
import styles from "../../styles/globals.css";

import Head from "next/head";
import "react-alice-carousel/lib/alice-carousel.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.19/tailwind.min.css"
        />
      </Head>

      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
