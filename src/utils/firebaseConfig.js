import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBI_xz46-rdZDDPss_kU5smc_lGqzwE6Q4",

  authDomain: "letchopro.firebaseapp.com",

  projectId: "letchopro",

  storageBucket: "letchopro.appspot.com",

  messagingSenderId: "728223557019",

  appId: "1:728223557019:web:c22f38f2c51271028bed3e",

  measurementId: "G-GHGYHGKEKF",
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
